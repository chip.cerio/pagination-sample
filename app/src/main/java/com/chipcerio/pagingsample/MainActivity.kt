package com.chipcerio.pagingsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ScrollListener.OnEndReachedListener {
    
    private var currentPage = 1
    
    private val itemsStream = PublishSubject.create<Int>()
    
    private val disposables = CompositeDisposable()
    
    private lateinit var textAdapter: TextAdapter

    private var hello = ""
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textAdapter = TextAdapter()
        
        recyclerview.apply {
            adapter = textAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            addOnScrollListener(ScrollListener(this@MainActivity))
        }
        
        initItemStream()
        itemsStream.onNext(currentPage)

        Toast.makeText(this, "Sample toast", Toast.LENGTH_SHORT).show()
    }
    
    private fun initItemStream() {
        itemsStream
            .observeOn(Schedulers.io())
            .doOnNext { println("page: $it") }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                textAdapter.addItems(generateItems())
            }, {
                println("Error")
            })
            .addTo(disposables)
    }
    
    override fun onEndReached() {
        println("end is reached")
        itemsStream.onNext(currentPage++)
    }
    
    private fun generateItems(): MutableList<String> {
        val list: MutableList<String> = mutableListOf()
        for (index in 1..20) {
            val text = "Item $index"
            println(index)
            list.add(text)
        }
        return list
    }
    
    override fun onStop() {
        super.onStop()
        disposables.clear()
    }
}

