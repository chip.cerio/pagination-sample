package com.chipcerio.pagingsample

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun <E> List<E>.clone(): List<E> = this.map { it }

fun Disposable.addTo(disposables: CompositeDisposable) = disposables.add(this)