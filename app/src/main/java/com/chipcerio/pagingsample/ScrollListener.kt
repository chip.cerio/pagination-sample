package com.chipcerio.pagingsample

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ScrollListener(private val listener: OnEndReachedListener) : RecyclerView.OnScrollListener() {
    
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val total = recyclerView.layoutManager?.itemCount
        val childCount = recyclerView.layoutManager?.childCount
        val lastVisibleItem = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
        
        val loading = false
        println("total: $total, child count: $childCount, " + "last visible: $lastVisibleItem, loading? ${!loading}")
    
        if (total != null) {
            if (!loading && total <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                listener.onEndReached()
            }
        }
    }
    
    interface OnEndReachedListener {
        fun onEndReached()
    }
    
    companion object {
        const val VISIBLE_THRESHOLD = 1
    }
}