package com.chipcerio.pagingsample

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TextAdapter : RecyclerView.Adapter<TextAdapter.ViewHolder>() {
    
    private var items: List<String> = emptyList()
    
    override fun getItemCount(): Int = items.size
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(android.R.layout.simple_list_item_1, parent, false))
    }
    
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }
    
    fun addItems(items: List<String>) {
        val newlist = mutableListOf<String>()
    
        newlist.addAll(this.items.clone())
        newlist.addAll(items)
        
        this.items = newlist
        
        notifyDataSetChanged()
    }
    
    // https://stackoverflow.com/a/49365796
    class ViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView) {
        
        private val textview: TextView = containerView.findViewById(android.R.id.text1)
        
        fun bind(text: String) {
            textview.text = text
        }
    }
}

